#include <cstdlib>
#include <string>
#include "BLEDevice.h"
#include <Arduino.h>
#include <WiFi.h>
#include <Thingspeak.h>

const uint channelID = 1183662;
const char *apiKey = "EVWZ0GAUHW5OUACA";    //Write API key from ThingSpeak
const char *ssid =  "liben1";     			
const char *password =  "abc12345";

static BLEUUID serviceUUID("56484aae-a8eb-4a97-ac19-a8ea6373e05a"); //Service UUID
static BLEUUID charUUID("2db34480-bce5-4bb7-9f56-55bd202317c5");	//characteristic UUID

static BLEAddress *pServerAddress;
static BLERemoteCharacteristic* pRemoteCharacteristic;
static boolean doConnect = false;
static boolean connected = false;

//Initalize Connection to the WiFi
void connectToWiFi(){
	delay(10);
	Serial.print("Connecting to ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);
	while(WiFi.status() != WL_CONNECTED){
		delay(500);
		Serial.println(".");
	}
	Serial.println("");
	Serial.println("Wifi connected");
}

//Sending data to Cloud 
static void sendDataToCloud(int sys, int dia, int pul){
	
	//Connect WiFi
	WiFiClient client;
	connectToWiFi();

	//ThinkSpeak Upload
	ThingSpeak.begin(client);
	delay(100);
	ThingSpeak.setField(1, sys);
	ThingSpeak.setField(2, dia);
	ThingSpeak.setField(3, pul);
	delay(100);
	ThingSpeak.writeFields(channelID, apiKey);
	Serial.println("Data sent to Cloud");
}

//Print out recieved Values
static void notifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, uint8_t* pData, size_t length, bool isNotify) {

	//print recieved Message
	Serial.println("Notify callback for characteristic ");
	Serial.print("Systolisch: ");
	Serial.println(pData[1]);
	Serial.print("Diastolisch: ");
	Serial.println(pData[3]);
	Serial.print("Puls: ");
	Serial.println(pData[14]);
	sendDataToCloud(pData[1],pData[3] ,pData[14]);
	delay(500);
	connected = false;
	Serial.println("Connectin closed. Restart device for new measurement.");
}




//Connect to Device
bool connectToServer(BLEAddress pAddress) {
	Serial.print("Forming a connection to ");
	Serial.println(pAddress.toString().c_str());

	BLEClient*  pClient = BLEDevice::createClient();
	Serial.println(" - Created client");

	// Connect to the remove BLE Server.
	pClient->connect(pAddress);
	Serial.println(" - Connected to server");

	// Obtain a reference to the service we are after in the remote BLE server.
	BLERemoteService* pRemoteService = pClient->getService(serviceUUID);
	if (pRemoteService == nullptr) {
		Serial.print("Failed to find our service UUID: ");
		Serial.println(serviceUUID.toString().c_str());
		return false;
	}
	Serial.println(" - Found our service");


	// Obtain a reference to the characteristic in the service of the remote BLE server.
	pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID);
	if (pRemoteCharacteristic == nullptr) {
		Serial.print("Failed to find our characteristic UUID: ");
		Serial.println(charUUID.toString().c_str());
		return false;
	}
	Serial.println(" - Found our characteristic");

	// Bind the notifyCallback method to the characteristic
	pRemoteCharacteristic->registerForNotify(notifyCallback);
	return true;
}


//Scan for BLE servers and find the first one that advertises the service we are looking for.
class MyAdvertisedDeviceCallbacks : 
public BLEAdvertisedDeviceCallbacks {
	
	
	//Called for each advertising BLE server.
	void onResult(BLEAdvertisedDevice advertisedDevice) {
		// Serial.print("BLE Advertised Device found: ");
		// Serial.println(advertisedDevice.toString().c_str());

		// We have found a device, let us now see if it contains the service we are looking for.
		if (advertisedDevice.haveServiceUUID() && advertisedDevice.getServiceUUID().equals(serviceUUID)) {
			Serial.print("Found our device!");
			advertisedDevice.getScan()->stop();

			pServerAddress = new BLEAddress(advertisedDevice.getAddress());
			doConnect = true;
			connected = true;
			

		}
		else{
			// Serial.println("Wrong Device");
			doConnect = false;
		} // Found our server
	} // onResult
}; // MyAdvertisedDeviceCallbacks



void setup() {
	Serial.begin(9600);

	Serial.println("Starting Arduino BLE Client application...");
	

}

void loop() {

	if(connected == false){
		BLEDevice::init("");
		Serial.println("Searching for Deviece...");
		BLEScan* pBLEScan = BLEDevice::getScan();
		pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
		pBLEScan->setActiveScan(true);
		pBLEScan->start(60);
	}
	else{
		if (doConnect == true) {
			if (connectToServer(*pServerAddress)) {
				Serial.println("We are now connected to the BLE Server.");
				connected = true;
			}
			else {
				Serial.println("We have failed to connect to the server; there is nothing more we will do.");
			}
			doConnect = false;
		}
	}
	
	
	
delay(100); // do a delay to keep things ticking over

}